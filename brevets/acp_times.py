"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""

import arrow
import math

# speed helper tables (from ACP)
maximum_speeds = [  
    (34, (0, 200)),
    (32, (200, 400)),
    (30, (400, 600)),
	(28, (600, 1000)),
    (26, (1000, 1300))
]

minimum_speeds = [
    (15, (0, 200)),
    (15, (200, 400)),
    (15, (400, 600)),
    (11.428, (600, 1000)),
	(13.333, (1000, 1300))
]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    return get_time(control_dist_km, brevet_dist_km, brevet_start_time, 'o')

def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    return get_time(control_dist_km, brevet_dist_km, brevet_start_time, 'c')

# logic all in one function, as they are very similar functions with only two differences
# plus, i got tired of reflecting bugfixes across the two functions
# two openCloseTypes: 'o' for open and 'c' for close
def get_time(control_dist_km, brevet_dist_km, brevet_start_time, openCloseType):
    time = 0
    delta = 0

    arrowtime = arrow.get(brevet_start_time).replace(tzinfo="US/Pacific")

    if control_dist_km == 0:
        if openCloseType == 'c':
            arrowtime = arrowtime.shift(hours=+1)
        return arrowtime.isoformat()
    
    if openCloseType == 'o':
        arrayToUse = maximum_speeds
    elif openCloseType == 'c':
        arrayToUse = minimum_speeds
    else:
        raise ValueError("Invalid openCloseType") 
    
    for speed, distanceRange in arrayToUse:
        if control_dist_km > distanceRange[0] and control_dist_km <= distanceRange[1]:
            # if time does not need to be rounded...
            if control_dist_km > 0 and ((((control_dist_km - brevet_dist_km) / control_dist_km) > 0.2) or (((control_dist_km - brevet_dist_km) / control_dist_km) < 0)):
                time += ((control_dist_km - distanceRange[0]) / speed) + delta
            # round the time to nearest brevet
            else:
                time += delta
            break
        else:
            # add time to clear distancerange at speed
            delta += (distanceRange[1] - distanceRange[0]) / speed

    hours = math.modf(time)[1]
    minutes = int(round(math.modf(time)[0] * 60))
    arrowtime = arrowtime.shift(hours=hours, minutes=minutes)

    return arrowtime.isoformat()