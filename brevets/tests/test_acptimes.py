from acp_times import open_time, close_time

import nose
import arrow
import logging

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

# All tests are from https://rusa.org/pages/acp-brevet-control-times-calculator

def test_20p():
    now = arrow.now()
    
    #should return true if over 20 percent from brevet time
    assert open_time(251, 200, now.isoformat()) == now.shift(hours=+6, minutes=+89).isoformat()

def test_openTime():
    now = arrow.now()

    #examples from https://rusa.org/pages/acp-brevet-control-times-calculator
    assert open_time(60, 200, now.isoformat()) == now.shift(hours=+1, minutes=+46).isoformat()
    assert open_time(120, 200, now.isoformat()) == now.shift(hours=+3, minutes=+32).isoformat()
    assert open_time(175, 200, now.isoformat()) == now.shift(hours=+5, minutes=+9).isoformat()
    assert open_time(205, 200, now.isoformat()) == now.shift(hours=+5, minutes=+53).isoformat()

def test_closeTime():
    now = arrow.now()

    assert close_time(60, 200, now.isoformat()) == now.shift(hours=+4, minutes=+0).isoformat()
    assert close_time(120, 200, now.isoformat()) == now.shift(hours=+8, minutes=+0).isoformat()
    assert close_time(175, 200, now.isoformat()) == now.shift(hours=+11, minutes=+40).isoformat()

def test_edges():
    # test where rates meet
    now = arrow.now()

    assert open_time(0, 1000, now.isoformat()) == now.shift(hours=+0, minutes=+0).isoformat()
    assert open_time(199, 1000, now.isoformat()) == now.shift(hours=+5, minutes=+51).isoformat()
    assert open_time(200, 1000, now.isoformat()) == now.shift(hours=+5, minutes=+53).isoformat()
    assert open_time(201, 1000, now.isoformat()) == now.shift(hours=+5, minutes=+55).isoformat()
    assert open_time(399, 1000, now.isoformat()) == now.shift(hours=+12, minutes=+6).isoformat()
    assert open_time(400, 1000, now.isoformat()) == now.shift(hours=+12, minutes=+8).isoformat()
    assert open_time(401, 1000, now.isoformat()) == now.shift(hours=+12, minutes=+10).isoformat()
    assert open_time(599, 1000, now.isoformat()) == now.shift(hours=+18, minutes=+46).isoformat()
    assert open_time(600, 1000, now.isoformat()) == now.shift(hours=+18, minutes=+48).isoformat()
    assert open_time(601, 1000, now.isoformat()) == now.shift(hours=+18, minutes=+50).isoformat()
    assert open_time(999, 1000, now.isoformat()) == now.shift(hours=+33, minutes=+3).isoformat()

    assert close_time(0, 1000, now.isoformat()) == now.shift(hours=+1, minutes=+0).isoformat()
    assert close_time(199, 1000, now.isoformat()) == now.shift(hours=+13, minutes=+16).isoformat()
    assert close_time(200, 1000, now.isoformat()) == now.shift(hours=+13, minutes=+20).isoformat()
    assert close_time(201, 1000, now.isoformat()) == now.shift(hours=+13, minutes=+24).isoformat()
    assert close_time(399, 1000, now.isoformat()) == now.shift(hours=+26, minutes=+36).isoformat()
    assert close_time(400, 1000, now.isoformat()) == now.shift(hours=+26, minutes=+40).isoformat()
    assert close_time(401, 1000, now.isoformat()) == now.shift(hours=+26, minutes=+44).isoformat()
    assert close_time(599, 1000, now.isoformat()) == now.shift(hours=+39, minutes=+56).isoformat()
    assert close_time(600, 1000, now.isoformat()) == now.shift(hours=+40, minutes=+0).isoformat()
    assert close_time(601, 1000, now.isoformat()) == now.shift(hours=+40, minutes=+5).isoformat()
    assert close_time(999, 1000, now.isoformat()) == now.shift(hours=+74, minutes=+55).isoformat()

def test_zero():
    now = arrow.now()
    assert open_time(0,200, now.isoformat()) == now.isoformat()
    assert close_time(0,200, now.isoformat()) == now.shift(hours=+1, minutes=+0).isoformat()
