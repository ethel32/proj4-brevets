"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    notes = ""

    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    brevet_dist_km = request.args.get("brevetDistKm", 200, type=int)
    begin_time = request.args.get("beginTime", "", type=str)
    date = request.args.get("beginDate", "", type=str)
    
    # formatting to be passed into open & close time
    start_iso = arrow.get("{} {}".format(date, begin_time)).isoformat()
    
    # get open and close times
    open_time = acp_times.open_time(km, brevet_dist_km, start_iso)
    close_time = acp_times.close_time(km, brevet_dist_km, start_iso)

    # error handling (warnings taken from the ACP calculator)
    percentWarn = (km > 0 and (((km - brevet_dist_km) / km) > 0.2))
    zeroWarn = (km == 0)

    if zeroWarn:
        notes = "The distance is zero"
    
    if percentWarn:
        notes = "The distance is 20 percent or longer than a standard brevet"

    return flask.jsonify(result={"open": open_time, "close": close_time, "notes": notes})

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
