# Project 4: Brevet time calculator with Ajax
Author: Ethel Arterberry

Reimplemented the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

# Purpose
This calculates the open and close times for standard ACP brevets with lengths of 200km, 300km, 400km, 600km and 1000km. It allows you to input checkpoints into a web inteface.

- Rounds seconds in a standard fashion.
- Error messages from the original calculator are kept and passed into the notes. If the control is between 0 and 20% longer than a brevet, it will be rounded to the nearest brevet. If it's 20% or longer, a message will be thrown